// Finding users with letter 'y' in their first OR last name
// show email and admin properties/fields

	db.users.find({
		$or:
		[
			{
				"firstName" : {$regex: 'y', $options: '$i'}
			},
			{
				"lastName" : {$regex: 'y', $options: '$i'}
			}
		]

		},
		{
			"_id" : 0, "email" : 1, "isAdmin" : 1
		})

// Finding users with letter 'e' in their firstName AND is an Admin
// show email and admin properties/fields


	db.users.find({
		$and:
		[
			{
				"firstName" : {$regex: 'e', $options: '$i'}
			},
			{
				"isAdmin" : true
			}
		]

		},
		{
			"_id" : 0, "email" : 1, "isAdmin" : 1
		})

// Finding products with letter 'x' in its name AND price greater than or equal to 50000

	db.products.find({
		$and: 
		[
			{
				"name" : {$regex: 'x', $options: '$i'}
			},
			{
				"price" : {$gte: 50000}
			}
		]
	}) 

// Update products with price less than 2000 to inactive

	db.products.updateMany({
		"price": {$lt: 2000}}, 
		{$set: {"isActive" : false}
	})

// Delete products with price greater than 20000
	db.products.deleteMany({
		"price" : {$gt: 20000}
	})